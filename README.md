### Spring, GraphQl, React, Apollo slide show
#### Install

To run this project you gonna need:  
JDK 11 - [download](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)  
Node js 11.9.0 - [download](https://nodejs.org/en/)   
Maven - [download](http://mirror.hosting90.cz/apache/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.zip)  

1. after downloading JDK install it  
2. after downloading Maven extract it  
3.  after downloading Node install it  
4. setup JAVA_HOME system variable to JDK home directory    
e.g. C:\Program files\Java\jdk-11.0.2
5. setup MAVEN_HOME system variable to Maven home directory   e.g. C:\apache-maven-3.6.0   
6. add MAVEN_HOME and JAVA_HOME to environment variables with `\bin` addition.   e.g. for Windows `%MAVEN_HOME%\bin` Tutorial is [here](https://www.mkyong.com/maven/how-to-install-maven-in-windows/)  
7. check if everything is correct by opening console and run `mvn -v`  
8. to start the server `cd slideshow` then `mvn spring-boot:run` for the first time it will take some time ...  
9. to start the client app cd `slideshow\src\main\web-app` and then `npm install react-scripts --save` and then `npm start`  

After application starts, you can use this data to login and be able to test slide show manager

username: "admin"  
password: "123"
