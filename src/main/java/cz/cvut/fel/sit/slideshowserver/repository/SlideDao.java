package cz.cvut.fel.sit.slideshowserver.repository;

import cz.cvut.fel.sit.slideshowserver.model.Slide;
import cz.cvut.fel.sit.slideshowserver.repository.common.BaseDao;
import org.springframework.stereotype.Repository;

/**
 * @author zarif
 * slide data access
 */
@Repository
public class SlideDao extends BaseDao<Slide> {
     protected SlideDao() {
          super(Slide.class);
     }
}

