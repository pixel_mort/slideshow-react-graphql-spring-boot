package cz.cvut.fel.sit.slideshowserver.configuration;

import cz.cvut.fel.sit.slideshowserver.model.User;
import cz.cvut.fel.sit.slideshowserver.repository.UserDao;
import cz.cvut.fel.sit.slideshowserver.resolver.Mutation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author zarif
 * creates test data
 */
@Component
public class DemoData {
     private final Mutation slideDao;
     private final UserDao userDao;


     @Autowired
     public DemoData(Mutation slideDao, UserDao userDao) {
          this.slideDao = slideDao;
          this.userDao = userDao;
     }


     @EventListener
     @Transactional
     public void appReady(ApplicationReadyEvent event) throws InterruptedException {
          slideDao.pushBack("First slide", "lorem ipsum", "https://picsum.photos/1500/500");
          slideDao.pushBack("Second slide", "lorem ipsum", "https://picsum.photos/1500/500");
          slideDao.pushBack("Third slide", "lorem ipsum", "https://picsum.photos/1500/500");

          User admin = new User("admin", "123");
          admin.setAdmin(true);
          userDao.persist(admin);
     }
}
