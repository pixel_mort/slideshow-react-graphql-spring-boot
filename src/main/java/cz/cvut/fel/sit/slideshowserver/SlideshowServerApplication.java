package cz.cvut.fel.sit.slideshowserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



/**
 * @author zarif
 * main class - starts server
 */
@SpringBootApplication
public class SlideshowServerApplication {

     public static void main(String[] args) {
          SpringApplication.run(SlideshowServerApplication.class, args);
     }
}

