package cz.cvut.fel.sit.slideshowserver.configuration;

import cz.cvut.fel.sit.slideshowserver.exception.GraphQLErrorAdapter;
import cz.cvut.fel.sit.slideshowserver.repository.SlideDao;
import cz.cvut.fel.sit.slideshowserver.repository.UserDao;
import cz.cvut.fel.sit.slideshowserver.resolver.Mutation;
import cz.cvut.fel.sit.slideshowserver.resolver.Query;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zarif
 * graphql configuration
 */
@Configuration
public class GraphQLErrorHandler {
     @Bean
     public GraphQLErrorHandler errorHandler() {
          return new GraphQLErrorHandler() {

               public List<GraphQLError> processErrors(List<GraphQLError> errors) {
                    List<GraphQLError> clientErrors = errors.stream()
                            .filter(this::isClientError)
                            .collect(Collectors.toList());

                    List<GraphQLError> serverErrors = errors.stream()
                            .filter(e -> !isClientError(e))
                            .map(GraphQLErrorAdapter::new)
                            .collect(Collectors.toList());

                    List<GraphQLError> e = new ArrayList<>();
                    e.addAll(clientErrors);
                    e.addAll(serverErrors);
                    return e;
               }


               protected boolean isClientError(GraphQLError error) {
                    return !(error instanceof ExceptionWhileDataFetching || error instanceof Throwable);
               }
          };
     }


     @Bean
     public Query query(SlideDao slideRepository, UserDao userDao) {
          return new Query(userDao, slideRepository);
     }


     @Bean
     public Mutation mutation(SlideDao slideRepository, UserDao userDao) {
          return new Mutation(userDao, slideRepository);
     }
}
