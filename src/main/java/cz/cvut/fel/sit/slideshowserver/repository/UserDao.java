package cz.cvut.fel.sit.slideshowserver.repository;

import cz.cvut.fel.sit.slideshowserver.model.User;
import cz.cvut.fel.sit.slideshowserver.repository.common.BaseDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zarif
 */
@Repository
public class UserDao extends BaseDao<User> {
     protected UserDao() {
          super(User.class);
     }

     @SuppressWarnings("unchecked")
     public User findByUsername(String username) {
          List<User> user;
          user = em
                  .createNamedQuery("User.findByUsername")
                  .setParameter("username", username)
                  .getResultList();
          if (user.size() == 0) {
               return null;
          } else {
               User result = user.get(0);
               return result;
          }
     }
}

