package cz.cvut.fel.sit.slideshowserver.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


/**
 * @author zarif
 * slide model
 */
@Entity
public class Slide {
     @Id
     @GeneratedValue
     private Integer slideID;
     private Integer orderNumber;

     private String slideTitle;
     private String slideDescription;
     private String uri;



     public Slide() {

     }


     public Slide(String slideTitle, String slideDescription, String uri) {
          this.slideTitle = slideTitle;
          this.slideDescription = slideDescription;
          this.uri = uri;
     }


     public Integer getSlideID() {
          return slideID;
     }


     public String getSlideTitle() {
          return slideTitle;
     }


     public String getSlideDescription() {
          return slideDescription;
     }


     public String getUri() {
          return uri;
     }


     public Integer getOrderNumber() {
          return orderNumber;
     }


     public void setSlideTitle(String slideTitle) {
          this.slideTitle = slideTitle;
     }


     public void setSlideDescription(String slideDescription) {
          this.slideDescription = slideDescription;
     }


     public void setUri(String uri) {
          this.uri = uri;
     }


     public void setOrderNumber(Integer orderNumber) {
          this.orderNumber = orderNumber;
     }
}
