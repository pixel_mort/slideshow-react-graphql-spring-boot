package cz.cvut.fel.sit.slideshowserver.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import cz.cvut.fel.sit.slideshowserver.model.Slide;
import cz.cvut.fel.sit.slideshowserver.repository.SlideDao;
import cz.cvut.fel.sit.slideshowserver.repository.UserDao;

public class Query implements GraphQLQueryResolver {
     protected final UserDao userDao;
     protected final SlideDao slideDao;


     public Query(UserDao userDao, SlideDao slideDao) {
          this.userDao = userDao;
          this.slideDao = slideDao;
     }

     public Iterable<Slide> getAllSlides() {
          return slideDao.findAll();
     }
}
