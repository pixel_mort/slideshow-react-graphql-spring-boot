package cz.cvut.fel.sit.slideshowserver.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * @author zarif
 */
@Entity
@NamedQuery(
        name = "User.findByUsername",
        query = "FROM User u WHERE u.username = :username"
)
public class User {
     @Id
     @GeneratedValue
     private Integer userID;

     private String
             username,
             password;

     private Boolean isAdmin = false;


     public User() {

     }


     public User(String username, String password) {
          this.username = username;
          this.password = password;
     }


     public Integer getUserID() {
          return userID;
     }


     public String getUsername() {
          return username;
     }


     public Boolean getAdmin() {
          return isAdmin;
     }


     public String getPassword() {
          return password;
     }


     public void setUsername(String username) {
          this.username = username;
     }


     public void setPassword(String password) {
          this.password = password;
     }


     public void setAdmin(Boolean admin) {
          isAdmin = admin;
     }
}