package cz.cvut.fel.sit.slideshowserver.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import cz.cvut.fel.sit.slideshowserver.model.Slide;
import cz.cvut.fel.sit.slideshowserver.model.User;
import cz.cvut.fel.sit.slideshowserver.repository.SlideDao;
import cz.cvut.fel.sit.slideshowserver.repository.UserDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Mutation implements GraphQLMutationResolver {
     protected final UserDao userDao;
     protected final SlideDao slideDao;


     public Mutation(UserDao userDao, SlideDao slideDao) {
          this.userDao = userDao;
          this.slideDao = slideDao;
     }


     @Transactional
     public Boolean login(String username, String password) {
          User user = userDao.findByUsername(username);
          if (user == null) {
               return false;
          }
          return user.getPassword().equals(password);
     }


     @Transactional
     public Boolean moveUp(Integer slideID) {
          Slide slide = slideDao.find(slideID);
          if (slide.getOrderNumber() == 1) {
               return true;
          } else {
               List<Slide> slides = slideDao.findAll();
               for (int i = 0; i < slides.size(); i++) {
                    Slide slideToDecrement = slides.get(i);
                    if (slideToDecrement.getOrderNumber() == slide.getOrderNumber() - 1) {
                         incrementSlideOrderNumber(slideToDecrement);
                         decrementSlideOrderNumber(slide);
                         break;
                    }
               }
          }
          return true;
     }


     public void decrementSlideOrderNumber(Slide slide) {
          slide.setOrderNumber(slide.getOrderNumber() - 1);
          slideDao.update(slide);
     }


     public void incrementSlideOrderNumber(Slide slide) {
          slide.setOrderNumber(slide.getOrderNumber() + 1);
          slideDao.update(slide);
     }


     @Transactional
     public Boolean moveDown(Integer slideID) {
          Slide slide = slideDao.find(slideID);
          if (slide.getOrderNumber() == slideDao.findAll().size()) {
               return true;
          } else {
               List<Slide> slides = slideDao.findAll();
               for (int i = 0; i < slides.size(); i++) {
                    Slide slideToIncrement = slides.get(i);
                    if (slideToIncrement.getOrderNumber() == slide.getOrderNumber() + 1) {
                         decrementSlideOrderNumber(slideToIncrement);
                         incrementSlideOrderNumber(slide);
                    }
               }
          }
          return true;
     }


     @Transactional
     public Slide pushBack(String postTitle, String postDescription, String uri) throws InterruptedException {
          TimeUnit.SECONDS.sleep(2);
          Slide slide = new Slide(postTitle, postDescription, uri);

          Integer slidesCount = slideDao.findAll().size();
          slide.setOrderNumber(slidesCount + 1);

          slideDao.persist(slide);
          printMessage(slide, "create");

          return slide;
     }


     @Transactional
     public Slide pushFront(String postTitle, String postDescription, String uri) throws InterruptedException {
          TimeUnit.SECONDS.sleep(2);
          Slide newSlide = new Slide(postTitle, postDescription, uri);

          incrementOrderNumber();

          newSlide.setOrderNumber(1);

          slideDao.persist(newSlide);
          printMessage(newSlide, "create");

          return newSlide;
     }


     public void incrementOrderNumber() {
          List<Slide> slides = slideDao.findAll();
          for (int i = 0; i < slides.size(); i++) {
               Slide slide = slides.get(i);
               slide.setOrderNumber(slide.getOrderNumber() + 1);
               slideDao.update(slide);
               break;
          }
     }


     public void decrementFromSlide(Integer val) {
          List<Slide> slides = slideDao.findAll();
          for (int i = 0; i < slides.size(); i++) {
               Slide slide = slides.get(i);
               if (slide.getOrderNumber() > val) {
                    slide.setOrderNumber(slide.getOrderNumber() - 1);
                    break;
               }
          }
     }


     @Transactional
     public Integer deleteSlide(Integer slideID) throws InterruptedException {
          TimeUnit.SECONDS.sleep(2);
          Slide slide = slideDao.find(slideID);
          if (slide != null) {
               printMessage(slide, "delete");
               decrementFromSlide(slide.getOrderNumber());
               slideDao.remove(slide);
               return slideID;
          } else {
               System.out.println("cant find slide with id: " + slideID);
               return slideID;
          }
     }


     @Transactional
     public boolean updateSlide(Integer slideID, String slideTitle, String slideDescription, String uri) throws InterruptedException {
          TimeUnit.SECONDS.sleep(2);
          Slide slide = slideDao.find(slideID);
          if (slide != null) {
               printMessage(slide, "update");
               slide.setSlideDescription(slideDescription);
               slide.setSlideTitle(slideTitle);
               slide.setUri(uri);

               slideDao.update(slide);
               return true;
          } else {
               System.out.println("cant find slide with id: " + slideID);
               return false;
          }
     }


     private void printMessage(Slide slide, String msgType) {
          System.out.println("-----------" + msgType + "-----------");
          System.out.println("slideID:            " + slide.getSlideID());
          System.out.println("slide title:        " + slide.getSlideTitle());
          System.out.println("slide description:  " + slide.getSlideDescription());
          System.out.println("order number:       " + slide.getOrderNumber());
          System.out.println("----------------------------");
     }
}
