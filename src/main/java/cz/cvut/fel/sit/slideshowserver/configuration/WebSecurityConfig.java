package cz.cvut.fel.sit.slideshowserver.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

/**
 * @author zarif
 * spring security
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
     @Override
     public void configure(HttpSecurity http) throws Exception {
          //@formatter:off
          http
             .authorizeRequests()
                  .anyRequest().permitAll()
             .and()
                  .formLogin()
                  .loginProcessingUrl("/login")
             .and()
                  .cors()
             .and()
                  .csrf().disable();
          //@formatter:on
     }


     @Bean
     CorsConfigurationSource corsConfigurationSource() {
          CorsConfiguration configuration = new CorsConfiguration();
          configuration = configuration.applyPermitDefaultValues();
          configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
          configuration.setAllowedMethods(Arrays.asList("GET", "POST", "UPDATE", "DELETE"));
          UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
          source.registerCorsConfiguration("/**", configuration);
          return source;
     }
}
