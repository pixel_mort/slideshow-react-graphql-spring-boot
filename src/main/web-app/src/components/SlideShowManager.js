import {DELETE_SLIDE, GET_ALL_SLIDE, MOVE_DOWN, MOVE_UP, PUSH_BACK, PUSH_FRONT, UPDATE_SLIDE} from "../graphqlQueries/queries";
import Mutation from "react-apollo/Mutation";
import React from "react";
import Modal from "react-bootstrap/Modal";

export default class SlideShowManager extends React.Component {
    render() {
        return (
            <div className={"container"}>
                <h3>Slide show manager</h3>
                <SlideShowManagerNewSlide/>
                <SlideShowManagerCards slides={this.props.slides}/>
            </div>
        )
    }
}

class SlideShowManagerNewSlide extends React.Component {

    state = {
        slideID: '',
        slideTitle: '',
        slideDescription: '',
        uri: "https://picsum.photos/1500/500"
    };


    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <React.Fragment>
                <div className={"card mb-2 mt-2"}>
                    <div className={"card-body"}>
                        <h5 className={"card-title"}>Create new slide</h5>
                    </div>
                </div>
                <div className={"card"}>
                    <div className={"card-body"}>
                        <form>
                            <div className={"row"}>
                                <div className={"col"}>
                                    <Form
                                        type={"text"}
                                        label={"Title"}
                                        handleChange={this.handleChange}
                                        name={"slideTitle"}
                                        value={this.state.slideTitle}
                                    />
                                </div>
                                <div className={"col"}>
                                    <Form
                                        type={"text"}
                                        label={"Description"}
                                        handleChange={this.handleChange}
                                        name={"slideDescription"}
                                        value={this.state.slideDescription}
                                    />
                                </div>
                            </div>
                            <Form
                                type={"text"}
                                label={"URL"}
                                handleChange={this.handleChange}
                                name={"uri"}
                                value={this.state.uri}
                            />
                        </form>
                        <Mutation
                            mutation={PUSH_FRONT}
                            variables={{
                                slideTitle: this.state.slideTitle,
                                slideDescription: this.state.slideDescription,
                                uri: this.state.uri
                            }}
                            update={
                                (cache, {data: {pushFront}}) => {
                                    const data = cache.readQuery({query: GET_ALL_SLIDE});
                                    SlideShowManagerNewSlide.incrementOrderNumber(data);
                                    data.getAllSlides.unshift(pushFront);
                                    cache.writeQuery({query: GET_ALL_SLIDE, data: data});
                                }}
                            optimisticResponse={
                                {
                                    pushFront: {
                                        slideID: -1,
                                        slideTitle: this.state.slideTitle,
                                        slideDescription: this.state.slideDescription,
                                        orderNumber: 1,
                                        uri: this.state.uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {pushFront => <button className={"btn btn-success"} onClick={pushFront}>Add to start</button>}
                        </Mutation>
                        <Mutation
                            mutation={PUSH_BACK}
                            variables={{
                                slideTitle: this.state.slideTitle,
                                slideDescription: this.state.slideDescription,
                                uri: this.state.uri
                            }}
                            update={
                                (cache, {data: {pushBack}}) => {
                                    const data = cache.readQuery({query: GET_ALL_SLIDE});
                                    data.getAllSlides.push(pushBack);
                                    cache.writeQuery({query: GET_ALL_SLIDE, data: data});
                                }}
                            optimisticResponse={
                                {
                                    pushBack: {
                                        slideID: -1,
                                        slideTitle: this.state.slideTitle,
                                        slideDescription: this.state.slideDescription,
                                        orderNumber: 100500,
                                        uri: this.state.uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {pushBack => <button className={"btn btn-success ml-2"} onClick={pushBack}>Add to end</button>}
                        </Mutation>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    static incrementOrderNumber(data) {
        for (let i = 0; i < data.getAllSlides.length; i++) {
            const slide = data.getAllSlides[i];
            slide.orderNumber += 1;
        }
    }
}

export const Form = (props) => {
    return (
        <div className="form-group">
            <label>{props.label}:</label>
            <input type={props.type} onChange={props.handleChange} className="form-control" name={props.name} value={props.value}/>
        </div>
    )
};

class SlideShowManagerCards extends React.Component {
    state = {
        show: false,
    };


    handleClose = () => {
        this.setState({show: false});
    };

    handleShow = () => {
        this.setState({show: true});
    };

    render() {
        const slides = this.props.slides;
        return (
            <React.Fragment>
                <div className={"card mb-2 mt-5"}>
                    <div className={"card-body"}>
                        <div className={"row"}>
                            <div className={"col"}>
                                <h5 className={"card-title"}>Slides manager</h5>
                            </div>
                            <div className={"col text-right"}>
                                <>
                                    <button className={"btn btn-info"} onClick={this.handleShow}>How to use?</button>

                                    <Modal show={this.state.show} onHide={this.handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>How to use</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <div className={"mr-2 ml-2"}>
                                                <div className={"row"}>
                                                    <button className={"btn btn-danger"}>DELETE</button>
                                                    <span className={"ml-2"}>click to delete the slide</span>
                                                    <div className={"alert alert-danger mt-2"}><strong>Warning!</strong> slide will be deleted
                                                        immediately without confirmation
                                                    </div>
                                                </div>
                                                <div className={"row"}>
                                                    <button className={"btn btn-warning"}>UPDATE</button>
                                                    <span className={"ml-2"}>enter new values and click to update the slide</span>
                                                    <div className={"alert alert-warning mt-2"}><strong>Warning!</strong> slide will be updated
                                                        immediately without confirmation
                                                    </div>
                                                </div>
                                                <div className={"row"}>
                                                    <span>
                                                    <button className={"btn btn-primary"}>⯅</button>
                                                    <button className={"btn btn-primary mr-2 ml-2"}>⯆</button>
                                                        <span>click to change slides order</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <button className={"btn btn-success"} onClick={this.handleClose}>Ok!</button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    slides.map((slide, key) =>
                        <div className={"card mb-2"} key={key}>
                            <div className={"card-body"}>
                                <SlideShowManagerCard slide={slide}/>
                            </div>
                        </div>
                    )
                }
            </React.Fragment>
        )
    }
}

class SlideShowManagerCard extends React.Component {

    state = {
        slideDescription: '',
        orderNumber: '',
        slideTitle: '',
        uri: ''
    };


    componentDidMount() {
        this.setState({
            slideDescription: this.props.slide.slideDescription,
            orderNumber: this.props.slide.orderNumber,
            slideTitle: this.props.slide.slideTitle,
            uri: this.props.slide.uri
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            slideDescription: nextProps.slide.slideDescription,
            orderNumber: nextProps.slide.orderNumber,
            slideTitle: nextProps.slide.slideTitle,
            uri: nextProps.slide.uri
        });
    }


    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    render() {
        const id = this.props.slide.slideID;
        const uri = this.state.uri;
        const title = this.state.slideTitle;
        const orderNumber = this.state.orderNumber;
        const description = this.state.slideDescription;

        return (
            <React.Fragment>
                <div className={"row"}>
                    <div className={"col-sm-2"}>
                        <h4 className={"card-title"}>Slide ID: {id}</h4>
                    </div>
                    <div className={"col-sm-6"}>
                        <Mutation
                            mutation={DELETE_SLIDE}
                            variables={{slideID: id}}
                            update={
                                (cache, {data: {deleteSlide}}) => {
                                    const data = cache.readQuery({query: GET_ALL_SLIDE});
                                    SlideShowManagerCard.deleteSlideFromCache(data, id);
                                    cache.writeQuery({query: GET_ALL_SLIDE, data: data});
                                }}
                            optimisticResponse={{deleteSlide: true}}
                        >
                            {deleteSlide => <button className={"btn btn-danger"} onClick={deleteSlide}>DELETE</button>}
                        </Mutation>
                        <Mutation
                            mutation={UPDATE_SLIDE}
                            variables={{
                                slideID: id,
                                slideTitle: title,
                                slideDescription: description,
                                uri: uri
                            }}
                            update={
                                (cache, {data: {updateSlide}}) => {
                                    const data = cache.readQuery({query: GET_ALL_SLIDE});
                                    SlideShowManagerCard.updateSlide(data, id, title, description, uri);
                                    cache.writeQuery({query: GET_ALL_SLIDE, data: data});
                                }}
                            optimisticResponse={
                                {
                                    updateSlide: {
                                        slideID: id,
                                        slideTitle: title,
                                        slideDescription: description,
                                        uri: uri,
                                        __typename: "Slide"
                                    }
                                }
                            }

                        >
                            {updateSlide => <button className={"btn btn-warning ml-2"} onClick={updateSlide}>UPDATE</button>}
                        </Mutation>
                    </div>
                    <div className={"col-sm-3 text-right"}>
                        <Mutation
                            mutation={MOVE_DOWN}
                            variables={{
                                slideID: id
                            }}
                            update={
                                (cache, {data: {moveDown}}) => {
                                    const data = cache.readQuery({query: GET_ALL_SLIDE});
                                    SlideShowManagerCard.moveDown(data, id);
                                    cache.writeQuery({query: GET_ALL_SLIDE, data: data});
                                }}
                            optimisticResponse={
                                {
                                    moveDown: {
                                        orderNumber: orderNumber + 1,
                                        slideDescription: description,
                                        slideTitle: title,
                                        slideID: id,
                                        uri: uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {moveDown => <button onClick={moveDown} className={"btn-sm btn btn-primary"}>⯆</button>}
                        </Mutation>
                        <Mutation
                            mutation={MOVE_UP}
                            variables={{
                                slideID: id
                            }}
                            update={
                                (cache, {data: {moveUp}}) => {
                                    const data = cache.readQuery({query: GET_ALL_SLIDE});
                                    SlideShowManagerCard.moveUp(data, id);
                                    cache.writeQuery({query: GET_ALL_SLIDE, data: data});
                                }}
                            optimisticResponse={
                                {
                                    moveUp: {
                                        orderNumber: orderNumber - 1,
                                        slideDescription: description,
                                        slideTitle: title,
                                        slideID: id,
                                        uri: uri,
                                        __typename: "Slide"
                                    }
                                }
                            }
                        >
                            {moveUp => <button onClick={moveUp} className={"btn-sm ml-2 btn btn-primary"}>⯅</button>}
                        </Mutation>
                    </div>
                    <div className={"col-sm-1 text-right"}>
                        <h4># {orderNumber}</h4>
                    </div>
                </div>
                <div className={"row"}>
                    <div className={"col"}>
                        <label>Title:</label>
                        <input className={"form-control"} onChange={this.handleChange} name={"slideTitle"} value={title}/>
                    </div>
                    <div className={"col"}>
                        <label>Description:</label>
                        <input className={"form-control"} onChange={this.handleChange} name={"slideDescription"} value={description}/>
                    </div>
                </div>
                <label className={"mt-2 "}>Url:</label>
                <div><input className={"form-control"} onChange={this.handleChange} name={"uri"} value={uri}/></div>
            </React.Fragment>
        );
    }

    //List methods
    static moveUp(data, id) {
        const slide = SlideShowManagerCard.find(data, id);
        if (slide.orderNumber === 1) {
            return;
        }
        for (let j = 0; j < data.getAllSlides.length; j++) {
            const slideToIncrement = data.getAllSlides[j];
            if (slideToIncrement.orderNumber === slide.orderNumber - 1) {
                slideToIncrement.orderNumber += 1;
                break;
            }
        }
        slide.orderNumber -= 1;
    }

    static moveDown(data, id) {
        const slide = SlideShowManagerCard.find(data, id);
        if (slide.orderNumber === data.getAllSlides.length) {
            return;
        }
        for (let j = 0; j < data.getAllSlides.length; j++) {
            const slideToIncrement = data.getAllSlides[j];
            if (slideToIncrement.orderNumber === slide.orderNumber + 1) {
                slideToIncrement.orderNumber -= 1;
                break;
            }
        }
        slide.orderNumber += 1;
    }

    static updateSlide(data, id, title, description, uri) {
        const slide = SlideShowManagerCard.find(data, id);
        slide.slideTitle = title;
        slide.slideDescription = description;
        slide.uri = uri;
    }

    static deleteSlideFromCache(data, id) {
        for (let i = 0; i < data.getAllSlides.length; i++) {
            const slide = data.getAllSlides[i];
            if (slide.slideID === id) {
                const val = slide.orderNumber;
                SlideShowManagerCard.decrementOrderNumberFromVal(data, val);
                data.getAllSlides.splice(i, 1);
                break;
            }
        }
    }

    static decrementOrderNumberFromVal(data, val) {
        for (let j = 0; j < data.getAllSlides.length; j++) {
            const slide = data.getAllSlides[j];
            if (slide.orderNumber > val) {
                slide.orderNumber -= 1;
            }
        }
    }

    static find(data, id) {
        for (let i = 0; i < data.getAllSlides.length; i++) {
            const slide = data.getAllSlides[i];
            if (slide.slideID === id) {
                return slide;
            }
        }
    }
}