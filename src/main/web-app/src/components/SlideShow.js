import React from "react";
import Carousel from "react-bootstrap/Carousel";

export default class SlideShow extends React.Component {

    render() {
        const slides = this.props.slides;

        return (
            <Carousel>
                {slides.map((slide, key) =>
                    <Carousel.Item key={key}>
                        <img src={slide.uri} className="slide" alt={"slide"}/>
                        <Carousel.Caption>
                            <h3>{slide.slideTitle}</h3>
                            <p>{slide.slideDescription}</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                )}
            </Carousel>
        )
    }
}