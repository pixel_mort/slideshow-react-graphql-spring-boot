import React from "react";
import ApolloClient from 'apollo-boost';
import ApolloProvider from "react-apollo/ApolloProvider";
import {GET_ALL_SLIDE, LOGIN} from "../graphqlQueries/queries";
import {Mutation, Query} from "react-apollo";
import SlideShow from "./SlideShow";
import SlideShowManager, {Form} from "./SlideShowManager";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Modal from "react-bootstrap/Modal";

const client = new ApolloClient({
    uri: "http://localhost:8080/graphql"
});

export default class App extends React.Component {
    state = {
        loggedIn: false,
        admin: false
    };


    onSubmit = async (login, data) => {
        const response = await login({variables: {username: data.username, password: data.password}});
        console.log(response.data.login);
        if (response.data.login === true) {
            this.setState({
                loggedIn: true,
                admin: true
            });
        }
    };

    render() {
        return (
            <ApolloProvider client={client}>
                <Query query={GET_ALL_SLIDE}>
                    {({loading, error, data}) => {
                        if (loading) return (
                            <div className="loading">Loading&#8230;</div>
                        );
                        if (error) return <h1>Error :(</h1>;

                        const slides = data.getAllSlides;
                        slides.sort(function (a, b) {
                            return a.orderNumber - b.orderNumber
                        });
                        return (
                            <React.Fragment>
                                <Navigation loggedIn={this.state.loggedIn} onSubmit={this.onSubmit}/>
                                <SlideShow slides={slides}/>
                                {this.state.loggedIn && this.state.admin ?
                                    <SlideShowManager slides={slides}/> :
                                    <React.Fragment/>
                                }
                            </React.Fragment>
                        )
                    }}
                </Query>
            </ApolloProvider>
        )
    }
}

class Login extends React.Component {
    state = {
        loggedIn: false,
        admin: false,
        username: "",
        password: ""
    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleSubmit = (login) => {
        this.props.onSubmit(login, this.state);
        this.props.handleClose();
    };

    render() {
        return (
            <div className={"container"}>
                <div className={"card mb-2 mt-3"}>
                    <div className={"card-body"}>
                        <h5>Login</h5>
                    </div>
                </div>
                <div className={"card mb-3"}>
                    <div className={"card-body"}>
                        <Form
                            label={"Username"}
                            handleChange={this.handleChange}
                            name={"username"}
                            value={this.state.username}
                            type={"text"}
                        />
                        <Form
                            label={"Password"}
                            handleChange={this.handleChange}
                            name={"password"}
                            value={this.state.password}
                            type={"password"}
                        />
                        <Mutation
                            mutation={LOGIN}
                        >
                            {login => <button onClick={() => this.handleSubmit(login)} className={"btn btn-success"}>Confirm</button>}
                        </Mutation>
                    </div>
                </div>
            </div>
        );
    }
}

class Navigation extends React.Component {

    state = {
        show: false,
    };


    handleClose = () => {
        this.setState({show: false});
    };

    handleShow = () => {
        this.setState({show: true});
    };

    render() {
        return (
            <Navbar bg={"dark"} variant={"dark"}>
                <Nav className="mr-auto">
                    {this.props.loggedIn ?
                        <React.Fragment/> :
                        <Nav.Link onClick={this.handleShow}>Login</Nav.Link>
                    }
                    <Modal show={this.state.show} onHide={this.handleClose}>
                        <Login handleClose={this.handleClose} onSubmit={this.props.onSubmit}/>
                    </Modal>
                </Nav>
            </Navbar>
        )
    }
}