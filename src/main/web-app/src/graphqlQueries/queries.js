import gql from "graphql-tag";

export const GET_ALL_SLIDE = gql`
    {
        getAllSlides {
            slideID
            slideTitle
            slideDescription
            orderNumber
            uri
        }
    }
`;


export const DELETE_SLIDE = gql`
    mutation PostMutation(
        $slideID: Int!
    ) {
        deleteSlide(
            slideID: $slideID
        )
    }
`;


export const PUSH_BACK = gql`
    mutation PostMutation(
        $slideTitle:        String!,
        $slideDescription:  String!,
        $uri:               String!
    ) {
        pushBack (
            slideTitle:         $slideTitle,
            slideDescription:   $slideDescription,
            uri:                $uri
        ) {
            slideID
            slideTitle
            slideDescription
            orderNumber
            uri
        }
    }
`;

export const PUSH_FRONT = gql`
    mutation PostMutation(
        $slideTitle:        String!,
        $slideDescription:  String!,
        $uri:               String!
    ) {
        pushFront (
            slideTitle:         $slideTitle,
            slideDescription:   $slideDescription,
            uri:                $uri
        ) {
            slideID
            slideTitle
            slideDescription
            orderNumber
            uri
        }
    }
`;


export const UPDATE_SLIDE = gql`
    mutation PostMutation(
        $slideID:           Int!,
        $slideTitle:        String!,
        $slideDescription:  String!,
        $uri:               String!
    ) {
        updateSlide (
            slideID:            $slideID,
            slideTitle:         $slideTitle,
            slideDescription:   $slideDescription,
            uri:                $uri
        )
    }
`;


export const MOVE_UP = gql`
    mutation PostMutation(
        $slideID:           Int!
    ) {
        moveUp (
            slideID:            $slideID
        )
    }
`;


export const MOVE_DOWN = gql`
    mutation PostMutation(
        $slideID:           Int!
    ) {
        moveDown (
            slideID:        $slideID
        )
    }
`;

export const LOGIN = gql`
    mutation PostMutation(
            $username: String!
            $password: String!
        ) {
            login (
                username: $username
                password: $password
                )
            }
`;
